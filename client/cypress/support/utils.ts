// Commong

export const getTitle = () => {
  return cy.get('ion-title');
};

// Login

export const getLoginButton = () => {
  return cy.get('.login-button');
};

export const getLoginUsernameField = () => {
  return cy.get('.username-input input');
};

export const getLoginPasswordField = () => {
  return cy.get('.password-input input');
};

export const getCreateAccountButton = () => {
  return cy.get('.create-account-button');
};

export const getLoadingOverlay = () => {
  return cy.get('.loading-wrapper');
};

// Home

export const getTabs = () => {
  return cy.get('.tab-button');
};

export const getSelectedTabIcon = () => {
  return cy.get('[aria-selected="true"] ion-icon');
};

export const getLogoutButton = () => {
  return cy.get('.logout-button');
};

// Register

export const getRegisterButton = () => {
  return cy.get('.register-button');
};

export const getEmailField = () => {
  return cy.get('.register-form .email-input input');
};

export const getRegisterUsernameField = () => {
  return cy.get('.register-form .username-input input');
};

export const getRegisterPasswordField = () => {
  return cy.get('.register-form .password-input input');
};

export const getConfirmPasswordField = () => {
  return cy.get('.register-form .confirm-password-input input');
};

// Chat

export const getChatListItems = () => {
  return cy.get('.chat-list ion-item');
};

export const getChatMessageField = () => {
  return cy.get('.chat-input');
};

export const getSendChatButton = () => {
  return cy.get('.send-chat-button');
};

// Notices

export const getNoticeListItems = () => {
  return cy.get('.notice-list .notice-cards');
};

export const getAddNoticeButton = () => {
  return cy.get('.add-notice-button');
};

export const getEditNoticeButton = () => {
  return cy.get('.edit-notice-button');
};

export const getDeleteNoticeButton = () => {
  return cy.get('.delete-notice-button');
};

export const getConfirmDeleteNoticeButton = () => {
  return cy.get('.alert-button-group button');
};

export const getSaveButton = () => {
  return cy.get('.save-notice-button');
};

export const getTitleField = () => {
  return cy.get('.title-input input');
};

export const getCloseButton = () => {
  return cy.get('page-add-notice .close-modal-button');
};

export const getModal = () => {
  return cy.get('ion-modal page-add-notice');
};
