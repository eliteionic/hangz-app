export interface RegistrationDetails {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}
