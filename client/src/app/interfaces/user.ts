export interface User {
  issued: number;
  expires: number;
  provider: string;
  ip: string;
  token: string;
  password: string;
  user_id: string;
  roles: string[];
  userDBs: {
    ['hangz-app']: string;
  };
}
