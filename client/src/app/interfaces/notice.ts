export interface Notice {
  _id: string;
  _rev: string;
  type: 'notice';
  title: string;
  message: string;
  author: string;
  dateCreated: string;
  dateUpdated: string;
}
