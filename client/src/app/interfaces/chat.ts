export interface Chat {
  _id: string;
  _rev: string;
  type: 'chat';
  message: string;
  author: string;
  dateCreated: string;
}
