import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNoticePage } from './add-notice.page';

const routes: Routes = [
  {
    path: '',
    component: AddNoticePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddNoticePageRoutingModule {}
