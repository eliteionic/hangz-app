import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { UserService } from './user.service';
import { DataService } from './data.service';
import { SERVER_ADDRESS } from '../../environments/environment';
import { Observable } from 'rxjs';

import { Credentials } from '../interfaces/credentials';
import { RegistrationDetails } from '../interfaces/registration-details';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private userService: UserService,
    private dataService: DataService,
    private navCtrl: NavController,
    private zone: NgZone
  ) {}

  authenticate(credentials: Credentials): Observable<User> {
    return this.http.post<User>(`${SERVER_ADDRESS}/auth/login`, credentials);
  }

  async logout(): Promise<void> {
    const headers = new HttpHeaders();

    headers.append(
      'Authorization',
      `Bearer ${this.userService.currentUser.token}:${this.userService.currentUser.password}`
    );

    this.http
      .post(`${SERVER_ADDRESS}/auth/logout`, {}, { headers: headers })
      .subscribe(() => {});

    try {
      await this.dataService.db.destroy();
      this.dataService.db = null;
      this.userService.saveUserData(null);
      this.navCtrl.navigateRoot('/login');
    } catch (err) {
      console.log(err);
      console.log('could not destroy db');
    }
  }

  register(details: RegistrationDetails): Observable<User> {
    return this.http.post<User>(`${SERVER_ADDRESS}/auth/register`, details);
  }

  validateUsername(username: string): Observable<Object> {
    return this.http.get(
      `${SERVER_ADDRESS}/auth/validate-username/${username}`
    );
  }

  validateEmail(email: string): Observable<Object> {
    const encodedEmail = encodeURIComponent(email);

    return this.http.get(
      `${SERVER_ADDRESS}/auth/validate-email/${encodedEmail}`
    );
  }

  async reauthenticate(): Promise<boolean> {
    if (this.dataService.db === null) {
      const userData = await this.userService.getUserData();

      if (userData !== null) {
        console.log(userData);

        const now = new Date();
        const expires = new Date(userData.expires);

        if (expires > now) {
          this.userService.saveUserData(userData);
          this.zone.runOutsideAngular(() => {
            this.dataService.initDatabase(userData.userDBs['hangz-app']);
          });

          return true;
        } else {
          throw new Error('Token expired');
        }
      } else {
        throw new Error('No user data available');
      }
    } else {
      return true;
    }
  }
}
