import { Injectable, NgZone } from '@angular/core';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';

import { Chat } from '../interfaces/chat';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private chatsSubject: BehaviorSubject<Chat[]> = new BehaviorSubject([]);

  constructor(public dataService: DataService, public zone: NgZone) {}

  init(): void {
    this.emitChats();

    this.dataService.db
      .changes({ live: true, since: 'now', include_docs: true })
      .on('change', (change: PouchDB.Core.ChangesResponseChange<Chat>) => {
        if (change.doc.type === 'chat' || change.deleted) {
          this.emitChats();
        }
      });
  }

  getChats(): BehaviorSubject<Chat[]> {
    return this.chatsSubject;
  }

  addChat(message: any): void {
    this.dataService.createDoc({
      message: message.message,
      author: message.author,
      dateCreated: message.dateCreated,
      type: 'chat',
    });
  }

  emitChats(): void {
    this.zone.run(async () => {
      let options = {
        include_docs: true,
        descending: true,
      };

      try {
        const data: PouchDB.Query.Response<Chat> = await this.dataService.db.query(
          'chats/by_date_created',
          options
        );

        const chats = data.rows
          .map((row) => {
            return row.doc;
          })
          .reverse();

        this.chatsSubject.next(chats);
      } catch (err) {
        console.log(err);
      }
    });
  }
}
