import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  public currentUser: User;
  private _storage: Storage;
  private storageInitialised: boolean = false;

  constructor(private storage: Storage) {}

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
    this.storageInitialised = true;
  }

  async saveUserData(data: User): Promise<void> {
    if (!this.storageInitialised) {
      await this.init();
    }
    this.currentUser = data;
    this._storage.set('hangzUserData', data);
  }

  async getUserData(): Promise<User> {
    if (!this.storageInitialised) {
      await this.init();
    }

    return this._storage.get('hangzUserData');
  }
}
