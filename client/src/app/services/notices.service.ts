import { Injectable, NgZone } from '@angular/core';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';

import { Notice } from '../interfaces/notice';

@Injectable({
  providedIn: 'root',
})
export class NoticesService {
  private noticesSubject: BehaviorSubject<Notice[]> = new BehaviorSubject([]);

  constructor(private dataService: DataService, private zone: NgZone) {}

  init(): void {
    this.emitNotices();

    this.dataService.db
      .changes({ live: true, since: 'now', include_docs: true })
      .on('change', (change: PouchDB.Core.ChangesResponseChange<Notice>) => {
        if (change.doc.type === 'notice' || change.deleted) {
          this.emitNotices();
        }
      });
  }

  getNotices(): BehaviorSubject<Notice[]> {
    return this.noticesSubject;
  }

  saveNotice(notice: any): void {
    if (notice.doc) {
      // Update existing doc with new values
      this.dataService.updateDoc({
        ...notice.doc,
        title: notice.title,
        message: notice.message,
        dateUpdated: notice.dateUpdated,
      });
    } else {
      // Create new doc
      this.dataService.createDoc({
        title: notice.title,
        message: notice.message,
        author: notice.author,
        dateCreated: notice.dateCreated,
        dateUpdated: notice.dateUpdated,
        type: 'notice',
      });
    }
  }

  deleteNotice(notice: Notice): void {
    this.dataService.deleteDoc(notice);
  }

  emitNotices(): void {
    this.zone.run(async () => {
      const options = {
        include_docs: true,
        descending: true,
      };

      try {
        const data: PouchDB.Query.Response<Notice> = await this.dataService.db.query(
          'notices/by_date_updated',
          options
        );

        const notices = data.rows.map((row) => {
          return row.doc;
        });

        this.noticesSubject.next(notices);
      } catch (err) {
        console.log(err);
      }
    });
  }
}
