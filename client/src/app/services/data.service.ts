import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb-browser';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  public db: PouchDB.Database = null;
  private remote: string;

  constructor() {}

  initDatabase(remote: string): void {
    this.db = new PouchDB('hangz-app', {
      auto_compaction: true,
    });

    this.remote = remote;
    this.initRemoteSync();
  }

  initRemoteSync(): void {
    const oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);

    let options = {
      live: true,
      retry: true,
      filter: 'app/after_date',
      query_params: { afterDate: oneWeekAgo },
    };

    this.db.sync(this.remote, options);
  }

  createDoc(doc: any): Promise<any> {
    return this.db.post(doc);
  }

  updateDoc(doc: any): Promise<any> {
    return this.db.put(doc);
  }

  deleteDoc(doc: any): Promise<any> {
    return this.db.remove(doc);
  }
}
