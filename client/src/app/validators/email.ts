import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class EmailValidator {
  private debouncer: ReturnType<typeof setTimeout>;

  constructor(private authService: AuthService) {}

  async checkEmail(control: FormControl): Promise<null | Object> {
    clearTimeout(this.debouncer);

    return new Promise((resolve) => {
      this.debouncer = setTimeout(() => {
        this.authService.validateEmail(control.value).subscribe(
          (res: any) => {
            if (res.ok) {
              resolve(null);
            }
          },
          (err) => {
            resolve({ emailInUse: true });
          }
        );
      }, 1000);
    });
  }
}
