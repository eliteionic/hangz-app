import { Component, NgZone, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { Credentials } from '../interfaces/credentials';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public username: string = '';
  public password: string = '';
  public failedAttempt: boolean = false;
  public loading: HTMLIonLoadingElement;

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private dataService: DataService,
    private userService: UserService,
    private loadingCtrl: LoadingController,
    private zone: NgZone
  ) {}

  async ngOnInit() {
    try {
      await this.authService.reauthenticate();
      this.navCtrl.navigateRoot('/home/tabs/notices');
    } catch (err) {
      console.log(err);
    }
  }

  async login(): Promise<void> {
    const loadingOverlay = await this.loadingCtrl.create({
      message: 'Authenticating...',
    });

    this.loading = loadingOverlay;
    this.loading.present();

    const credentials: Credentials = {
      username: this.username,
      password: this.password,
    };

    this.authService.authenticate(credentials).subscribe(
      async (user) => {
        console.log(user);

        if (typeof user.token !== 'undefined') {
          this.failedAttempt = false;

          this.zone.runOutsideAngular(() => {
            this.dataService.initDatabase(user.userDBs['hangz-app']);
          });

          this.userService.saveUserData(user);

          await this.loading.dismiss();
          this.navCtrl.navigateRoot('/home/tabs/notices');
        }
      },
      (err) => {
        this.loading.dismiss();
        this.failedAttempt = true;
        console.log(err);
      }
    );
  }
}
