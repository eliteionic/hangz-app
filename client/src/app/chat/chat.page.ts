import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  NavController,
  LoadingController,
  IonContent,
  IonList,
} from '@ionic/angular';
import { Chat } from '../interfaces/chat';
import { ChatService } from '../services/chat.service';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { skip } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  @ViewChild(IonContent, { static: false }) contentArea: IonContent;
  @ViewChild(IonList, { read: ElementRef, static: false }) chatList: ElementRef;

  public chats: Chat[] = [];
  public message: string = '';
  private loading: HTMLIonLoadingElement;
  private mutationObserver: MutationObserver;

  constructor(
    private chatService: ChatService,
    public userService: UserService,
    private authService: AuthService,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController
  ) {}

  async ngOnInit() {
    this.loading = await this.loadingCtrl.create({
      message: 'Authenticating...',
    });

    this.loading.present();

    try {
      await this.authService.reauthenticate();
      this.loading.dismiss();

      this.chatService.init();

      this.chatService
        .getChats()
        .pipe(skip(1))
        .subscribe((chats) => {
          this.chats = chats;

          if (this.chats.length === 0) {
            this.chats.push({
              _id: '',
              _rev: '',
              dateCreated: '',
              type: 'chat',
              author: 'Hangz Admin',
              message:
                'Looks like nobody is around. Type a message below to start chatting!',
            });
          }
        });
    } catch (err) {
      console.log(err);
      this.loading.dismiss();
      this.navCtrl.navigateRoot('/login');
    }

    this.mutationObserver = new MutationObserver((mutations) => {
      setTimeout(() => {
        this.newChatAdded();
      }, 20);
    });

    this.mutationObserver.observe(this.chatList.nativeElement, {
      childList: true,
    });
  }

  trackById(index: number, chat: Chat): string {
    return chat._id;
  }

  newChatAdded(): void {
    this.contentArea.scrollToBottom();
  }

  addChat(): void {
    if (this.message.length > 0) {
      let iso = this.getDateISOString();

      this.chatService.addChat({
        message: this.message,
        author: this.userService.currentUser.user_id,
        dateCreated: iso,
      });

      this.message = '';
    }
  }

  getDateISOString(): string {
    return new Date().toISOString();
  }

  logout() {
    this.authService.logout();
  }

  handleKeyup(ev: KeyboardEvent) {
    if (ev.key === 'Enter') {
      this.addChat();
    }
  }
}
