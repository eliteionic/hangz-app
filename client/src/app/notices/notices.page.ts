import { Component, OnInit } from '@angular/core';
import {
  AlertController,
  ModalController,
  ActionSheetController,
  LoadingController,
  NavController,
  AnimationController,
  Animation,
} from '@ionic/angular';
import { AddNoticePage } from '../add-notice/add-notice.page';
import { Notice } from '../interfaces/notice';
import { NoticesService } from '../services/notices.service';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { skip } from 'rxjs/operators';

@Component({
  selector: 'app-notices',
  templateUrl: './notices.page.html',
  styleUrls: ['./notices.page.scss'],
})
export class NoticesPage implements OnInit {
  public notices: Notice[] = [];
  private loading: HTMLIonLoadingElement;

  constructor(
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController,
    private animationCtrl: AnimationController,
    private noticesService: NoticesService,
    private authService: AuthService,
    public userService: UserService
  ) {}

  async ngOnInit() {
    this.loading = await this.loadingCtrl.create({
      message: 'Authenticating...',
    });

    this.loading.present();

    try {
      await this.authService.reauthenticate();
      this.loading.dismiss();

      this.noticesService.init();

      this.noticesService
        .getNotices()
        .pipe(skip(1))
        .subscribe((notices) => {
          this.notices = notices;

          if (this.notices.length === 0) {
            this.notices.push({
              _id: '',
              _rev: '',
              dateCreated: '',
              dateUpdated: '',
              type: 'notice',
              author: 'Hangz Admin',
              title: 'Welcome!',
              message:
                "Looks like there aren't any notices yet. Click the '+' symbol to add one.",
            });
          }
        });
    } catch (err) {
      this.loading.dismiss();
      this.navCtrl.navigateRoot('/login');
    }
  }

  async openAddNoticePage(notice?: Notice): Promise<void> {
    const modal = await this.modalCtrl.create({
      component: AddNoticePage,
      componentProps: {
        notice: notice,
      },
    });

    modal.present();
  }

  async deleteNotice(
    elementBeingDeleted: HTMLElement,
    notice: Notice
  ): Promise<void> {
    const confirm = await this.alertCtrl.create({
      header: 'Delete this notice?',
      message: 'Deleting this notice will remove it permanently.',
      buttons: [
        {
          text: 'Delete',
          handler: async () => {
            const animation: Animation = this.animationCtrl
              .create()
              .addElement(elementBeingDeleted)
              .duration(500)
              .easing('ease-out')
              .fromTo('opacity', '1', '0');

            await animation.play();

            this.noticesService.deleteNotice(notice);
          },
        },
        {
          text: 'Keep it',
        },
      ],
    });

    confirm.present();
  }

  async openNoticeActions(ev: MouseEvent, notice: Notice): Promise<void> {
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Edit',
          icon: 'create',
          handler: () => {
            this.openAddNoticePage(notice);
          },
        },
        {
          text: 'Delete',
          role: 'destructive',
          icon: 'trash',
          handler: () => {
            const cardElement = (<HTMLIonButtonElement>ev.target).parentElement
              .parentElement;

            this.deleteNotice(cardElement, notice);
          },
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
        },
      ],
    });

    actionSheet.present();
  }

  logout(): void {
    this.authService.logout();
  }
}
