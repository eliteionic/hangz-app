import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoticesPageRoutingModule } from './notices-routing.module';

import { NoticesPage } from './notices.page';
import { AddNoticePage } from '../add-notice/add-notice.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, NoticesPageRoutingModule],
  declarations: [NoticesPage, AddNoticePage],
})
export class NoticesPageModule {}
