export const environment = {
  production: true,
};

export const SERVER_ADDRESS = 'https://hangz-app.herokuapp.com';
