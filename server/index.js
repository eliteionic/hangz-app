var express = require("express");
var https = require("https");
var logger = require("morgan");
var cors = require("cors");
var SuperLogin = require("@sl-nx/superlogin-next");

var app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

var dbServerConfig = {
  host: process.env.COUCHDB_HOST,
  userDB: "hangz-app-users",
  couchAuthDB: "_users",
};

if (process.env.COUCHDB_IAM_API_KEY) {
  // Configure DB for Cloudant
  dbServerConfig = {
    ...dbServerConfig,
    protocol: "https://",
    cloudant: true,
    iamApiKey: process.env.COUCHDB_IAM_API_KEY,
  };
} else {
  // Configure DB for local development
  dbServerConfig = {
    ...dbServerConfig,
    protocol: "http://",
    user: process.env.COUCHDB_USER,
    password: process.env.COUCHDB_PASS,
  };
}

var config = {
  dbServer: dbServerConfig,
  security: {
    maxFailedLogins: 5,
    lockoutTime: 600,
    tokenLife: 604800, // one week
    loginOnRegistration: true,
  },
  mailer: {
    fromEmail: "gmail.user@gmail.com",
    options: {
      service: "Gmail",
      auth: {
        user: "gmail.user@gmail.com",
        pass: "userpass",
      },
    },
  },
  userDBs: {
    defaultDBs: {
      shared: ["hangz-app"],
    },
    model: {
      ["hangz-app"]: {
        permissions: ["_reader", "_writer", "_replicator"],
      },
    },
  },
  providers: {
    local: true,
  },
};

// Initialize SuperLogin
var superlogin = new SuperLogin(config);

// Mount SuperLogin's routes to our app
app.use("/auth", superlogin.router);

app.listen(process.env.PORT || 8080);
